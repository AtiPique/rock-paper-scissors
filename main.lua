#!/usr/local/bin/lua

function intInput()
  io.write("> ")
  n = io.read("*n")

  while(n < 0 or n > 4)
    do
      io.write("> ")
      n = io.read("*n")
    end

  return n
end

elements = {"rock", "paper", "scissors"}

local playerOne, playerTwo

for i = 1, 3
  do
      print(i, ":", elements[i])
  end

print("Player one (1 - 3)")
playerOne = intInput()

print("Player two (1 - 3)")
playerTwo = intInput()

if playerOne == playerTwo then
  print("Ex aequo !")
elseif playerOne == 1 and playerTwo == 2 then
  print("Player two wins !")
elseif playerOne == 2 and playerTwo == 1 then
  print("Player One wins !")
elseif playerOne == 2 and playerTwo == 3 then
  print("Player two wins !")
elseif playerOne == 3 and playerTwo == 2 then
  print("Player One wins !")
elseif playerOne == 3 and playerTwo == 1 then
  print("Player two wins !")
elseif playerOne == 1 and playerTwo == 3 then
  print("Player One wins !")
end
